ARES-Setup Changelog Dec 16 2020

1. Added support for Libre Computer boards via mali-drm-gles2
2. Jetson Nano support was added earlier this year, along with Odroid H2 and H2+, Nano Pi Fire 3, Odroid N2+, Raspberry PI 4, Atomic Pi, and X86 PC.
3. Amiberry ported to Libre Computer devices.
4. Flycast ported to Libre Computer devices
5. Splashvideos fixed on Libre Computer devices.
6. Flycast ported to Jetson Nano (earlier in the year)
7. Mupen ported to Jetson Nano (earlier in the year)

ARES-SETUP Changelog May 23 2020

1. enabled binary installs for rpi4 xu4 and n2
2. change source to eduke32
3. update audiosettings.sh for pi4
4.  helpers - rework hasPackage logic to fix some cases where it failed
This was adjusted previously to resolve an issue with a sdl2 "ne" check - but broke another case where
hasPackage usbmount "0.0.24" would return ok when usbmount had been previously installed but removed.
We now always do compareVersions when a version / boolean operator is used, but avoid comparing against
any versions of removed packages by blanking the version string of any removed package.
This should now handle installed/not installed packages and boolean operators correctly
5. Merge pull request #3150 from joolswills/usbmount_uid_fix
usbromservice - fix mounting ext3/ext4 partitions

ARES-SETUP Changelog May 23 2020

1. Added Hypseus laserdisc emulator Modernized sdl2 fork of daphne

ARES-SETUP Changelog May 22 2020

1. lr-picodrive: fix syntax errors
2. lr-caprice32: create core-options config for gx4000
3. lr-vice-xplus4. set core option to "Plus4 NTSC" (DO NOT UPDATE TO THIS VERSION. Loading issues)
4. zesarux set repository branch to master


ARES-SETUP Changelog May 21 2020

1. residualvm - remove --enable-keymapper option (no longer valid)
2. lr-bsnes: fix compiler version detection
3. Adjust GCC comparisons to use major version only to fix version comparing
Older GCC on at least Debian used to output a 3 part version like 6.3.0 with gcc -dumpversion
Newer GCC / OS seems to only output the major version which broke comparisons as dpkg will consider a version such as "6" to be lower than 6.0.0
4. lr-mame / lr-mess - gcc 7 is needed
5. runcommand.sh add missing file extension in mtx512 bios check.
6. lr-bluemsx: iniSet "bluemsx_vdp_synctype" "60Hz" "$core_config" for msx msx2 msx2+ spectravideo
7. lr-bluemsx iniSet "bluemsx_vdp_synctype" "Auto" "$core_config" and iniSet "bluemsx_msxtype" "Auto" "$core_config" for msxturbo
8. lr-mess: enabled softlists for all systems and modes
9. eduke32 and ionfury: found a source archive and reenabled.
10. lr-beetle-saturn: disable for all arm and aarch64
11. lr-picodrive: set as default for 32x and pico since it's the only core that supports them.
12. lr-mess and platforms.cfg: added apple2e (as a seperate system from apple2.) req. apple2.zip apple2ee.zip apple2e.zip apple2p.zip BIOS files in /roms/apple2e


ARES-SETUP Changelog May 16 2020

1. Retroarch 1.86
2. packages.sh version 4.6.1
3. lr-bsnes: update to latest version
4. misc typos and config errors fixed
5. added .zip and .m3u for lr-hatari
6. added .zip and corrected BIOS path for crvision



ARES-SETUP Changelog May 7 2020

1. runcommand.sh: added MESS systems to BIOS check
2. gemrb.sh disabled for odroids and rockpro64
3. lr-atari800.sh: create core-options files for 400/800 130XE and 5200 so the core starts with the appropriate machine
4. lr-parallel-n64 does not work on xu4 all of a sudden. (Tested Mario Kart 64 and got an illegal instruction)
5. Revert "lr-parallel-n64 - revert 11c1ae33 to fix segfault on exit on arm"
6. solarus - fix missing "false" case in wrapper script
7. autostart.sh added missing ogst lcd code for XU4
8. ecwolf.sh lowercase
9. reenable supertux on n2 and configured it.


ARES-SETUP Changelog May 5 2020

1. lr-mess mess-dev: removed non working or duplicate systems
2. splashscreen.sh: fix missing screen size arguments for xu4
3. platforms.cfg add intvecs

ARES-SETUP Changelog May 4 2020

1. mess-dev.sh mame-dev.sh fix return files.
2. runcommand: use a correct regexp for grep to match the default modeline
3. runcommand: improve current video mode detection on KMS
4. setup - fix logic on updating section packages aborting on the first uninstalled package
5. Merge pull request #3125 from psyke83/sixaxis_and_updateall Sixaxis fixes for bare installation
6. lr-parallel-n64.sh fix build for rpi4
7. N64: Disabled hybrid upscaling filter (causes slowdown on RPI4)
8. eduke32 and ionfury source offline so disabled for now.
9. disabled yabasanshiro on rpi
10. video splashscreens should work on pi4 xu4 rp64 and n2
11. disable supermodel and play2
12. sdl2trs change build option to nox


ARES-Setup Changelog 02-05-2020

1. lr-flycast revert to make on nvidia jetson nano and don't disable lto due break if you disable lto.
2 - add nvidia jetson nano for mupen64plus standalone
3 - add nvidia jetson nano  for lr-paralell-n64
4 - lr-craft disable for opengl es the port it's broken only working on opengl desktop "gl"
5 - Fix mising fi in sdltrs.sh 

ARES-SETUP Changelog May 1 2020

1. packages.sh: increment package number limits
2. lr-neocd is now default for neogeo-cd
3. fix flycast lr-parallel-n64 and mupen64plus for jetson-nano
4. add lr-dosbox-svn.sh
 

ARES-SETUP Changelog Apr 30 2020

1. added sdl2trs sdltrs trs-80 emulator ported to sdl2
2. lr-mame.sh: removed extra } on line 65
3. skyscraper.sh: Updates the script to conform with the new '--flags FLAG1,FLAG2' flags #3119
4. Duckstation fix odroid n2 and some rewrites.


ARES-SETUP Changelog Apr 29 2020

1. dosbox - show larger freesize for C drive for installers that check free space.
2. retroarch - setting `video_smooth` is no longer necessary
3. lr-bluemsx: Force correct system for msx msx2 msx2+ msxturbor and spectravideo


ARES-SETUP Changelog Apr 28 2020

1. Update Version to 4.6
2. Enable software lists for ql-cass radio86-cass and pegasus-cass
3. lr-vice-* makefile name changed upstream
4. srb2 lock to version 2.22 add missing patch.pk3 asset

ARES-SETUP Changelog Apr 27 2020
1. runcommand: do not build mesa-drm for jetson-nano


ARES-SETUP Changelog Apr 26 2020
1. Fan Control added for xu4*
2. Runcommand.sh: OGST Case support restored for XU4*
3. Change jupiter-ace name to jupace.
* Will not work on current build. Wait for next alpha image.


ARES-SETUP Changelog Apr 24 2020
1. Fix more n2 launch issues
2. Sync With RetroPie 4.5.20

ARES-SETUP Changelog Apr 17 2020

1. openttd: fix launching on n2
2. disable lzdoom on n2 for now
3. mysticmine: add XINIT:  need retesting on xu4
4. configs: incorrect filenames and add new systems
5. platforms.cfg: more systems and extensions
6. retroarch update to 1.85


ARES-SETUP Changelog Apr 14 2020

1. ports- add devilutionx (from meveric) for XU4 and n2
2. ports- jumpnbump fixed permissions issue
3. fix permissions for lr-stella and lr-smsplus-gx
4. disable lr-craft for all for now.
5. disable openjazz for odroid n2
6. disable lmarbles
7. disable mysticmine for odroids. (will not run fullscreen)
8. Joystick-selection: work around for js-onstart.sh not getting created like it should.

ARES-SETUP Changelog Apr 12 2020

1. Removed commodore maxmachine (just a very shortlived stripped down c64. c64 is compatible)
2. disabled ags on n2
3. disabled wolf4sdl on n2
4. disabled barrage on n2
5. reenabled splitwolf (upstream dmca issue resolved) 
6 Disabled all saturn emulators except yabasanshiro for arm and aarch64
7. bump to version 4.5.19
8. fix solarus for rpi
9. update joy2key to python3
10. added ecwolf, lr-2048, lr-cannonball, lr-stella, lr-cannonball, lr-chailove, lr-craft, lr-freej2me, lr-lutro, lr-meteor, lr-mu, lr-openlara, lr-retro8, lr-smsplus-gx, lr-tic80, lr-uzebox, lr-vbam, openjazz, supermodel  
tested ecwolf, lr-chailove, lr-lutro, openjazz working on the xu4 lr-craft doesn't work on xu4 lr-openlara will not build nor will supermodel.

ARES-SETUP Changelog Apr 11 2020
1. Recomendations for citra needed image .3ds format and decrypted in romdir 3DS folder.
2  Duckstation psx emulator The configuration hard located in /.local/share/duckstation and I create /.local/share/duckstation/bios symbolic link to ARES BIOS folder
   Note : for keyboard users can't remap limitation for now not option yet in the emulator. 
   The default bindings :
   D-Pad: W/A/S/D
   Triangle/Square/Circle/Cross: Numpad8/Numpad4/Numpad6/Numpad2
   L1/R1: Q/E
   L2/L2: 1/3
   Start: Enter
   Select: Backspace
   Hotkeys:
   Escape: Power off console
   ALT+ENTER: Toggle fullscreen
   Tab: Temporarily disable speed limiter
   Pause/Break: Pause/resume emulation
   Page Up/Down: Increase/decrease resolution scale in hardware renderers
   End: Toggle software renderer
   
 3. Added lr-thepowdertoy https://github.com/libretro/ThePowderToy (ports)
 4. Added lr-mesen nes emulator
 5. Change rom folder names for astrocade bbcmicro watara supervision, now astrocde bbcbp128 svision to better support lr-mess
 6. removed microvision RCA studio 2 (Don't work well or at all.)
 7. fix lr-mesen syntax error and add system dendy (Russian Famiclone)
 8. Fix lr-mess to copy over custom configurations and core options.
 9. platforms.cfg add system dendy
 

ARES-Setup Changelog  Apr 10 2020

Changes:

1. platforms.cfg: fixed typos, missing fullnames and missing extensions
2. advmame*.sh fixed config functions so MESS systems don't get advmame 0.94 or 1.4 entries
3. xroar.sh: removed n2 option (not required)

ARES-Setup Changelog  Apr 4 2020

Changes:

1. openomf2097: manually installed unrar due to it being unavailable on Raspbian
2. set default for cps 1,2,3 to fbneo
3. Arcade defaults to lr-mame2003
4. runcommand fix channelf BIOS check

ARES-Setup Changelog  Apr 2 2020

Changes:

1. runcommand: Fixed BIOS Check function for intellivision: typo
2. Fixed launching commands for n2 and xu4
3. added 1x options for yabasanshiro 
4. made yabasanshiro default saturn emulators
5. disabled lr-bsnes, lr-beetle-saturn lr-kronos for armhf and odroid-n2 
6. disabled dosbox sdl1 for n2
7. disabled atari800 for n2
8. disabled daphne for n2

ARES-Setup Changelog  Mar 25 2020

In sync with RetroPie 4.5.18

Changes:

1. runcommand: Fixed BIOS Check function

2. added fixmali function for rp4 and n2 to get_depends functions in each script module


ARES-Setup Changelog  Mar 23 2020

In sync with RetroPie 4.5.17

Changes:

1. srb2: add missing libpng dependency

2. lr-hatari: fix linkage on Ubuntu 18.04 & friends

3. yquake2 - added libcurl4-openssl-dev dependency

4. amiberry - removed unused libguichan-dev dependency amiberry - fix building on odroid c1

5. Merge pull request #3031 from joolswills/packaging Packaging / setup tweaks


ARES-Setup Changelog  Mar 20 2020

In sync with RetroPie 4.5.17

Changes:

1. Remove lr-redream 

2. lr-mame2003 - adjust configure logic to avoid copying files / configuration on removal

3. lr-desmume - added missing dependency

4. ares_packages - override return code if __ERRMSGS is set and print contents

 * this works around calling some module functions manually that may call others that return errors (eg setup basic_install) - only affects commandline usage
 * output __ERRMSGS at the end of ares_packages.sh as we do for __INFMSGS (setup clears these after display so only affects commandline usage)
 
5. sdl1 / sdl2 - add __binary_url functions and rework packaging logic


ARES-Setup Changelog  Mar 19 2020

In sync with RetroPie 4.5.17

Changes:

1. Removed unused tools folder

2. system.sh system - workaround for older kernels without MemAvailable via /proc/meminfo

3. image.sh image - consistent quoting / one case of missing quotes for $chroot

4. stats: modify the pkgflags stats logic after the flags reorganization

5. stats: extended to support rockpro64 odroid xu4 kms/drm odroid n2 and jetson-nano

6. image.sh image - enable for arm and various fixes

 * only use qemu-arm-static for x86
 * remove qemu-arm-static from chroot in _deinit function
 * remove leftover comment that should have been removed

ARES-Setup Changelog  Mar 17 2020

In sync with RetroPie 4.5.17

Changes: 

1. admame.sh: advmame: disable multithreading temporary to resolve a crash

2. lr-nestopia.sh nestopia: remove the board database file from the package installation

3. skyscraper: don't use predefined variable names for the config dir

4. runcommand.sh  Launching and exit video functions restored, BIOS check function restored and extended. (Currently checks all systems that require a BIOS except MSX and systems running through advmess and lr-mess)

ARES-Setup Changelog  Mar 13 2020

In sync with RetroPie 4.5.17

Function Changes

1. See RetroPie Commits from 3/12/20 to 3/13/20 as of 15:44 PDT 


ARES-Setup Changelog  Mar 11 2020

In sync with RetroPie 4.5.15

Function Changes

1. helpers.sh packages.sh:  Merge pull request #3012 from joolswills/getDepends_rework

2. Retroarch: Merge pull request #3015 from joolswills/retroarch_corruption_fix

3. helpers.sh builder.sh system.sh: Merge pull request #3006 from joolswills/system_reorg + added an ra platform definition to prevent xserver-xorg-legacy from being removed. X apps under the xu4 rp64 and n2 won't launch properly without it.

4. golang.sh: change version to go1.10.1.linux and add aarch64 option. (fixes sselph scraper on arm64)

5. moonlight.sh XU4 and N2 need -platform sdl flag to execute

6. dosbox.sh: dosbox - avoid infinite loop on startup if timidity fails  and - incorrect $ on i incremement

7. burgerspace.sh: REMOVED due to it not working reliably without full desktop and it sucks

8. prboom-plus.sh: REMOVED. Can't get it to build on arm and we've got plenty of existing options for Doom


ARES-Setup Changelog  Mar 9 2020

In sync with RetroPie 4.5.14

Function Changes

1. system.sh -Vero4k is now mali

2. helpers.sh - ignore mali-fbdev on vero4k and odroid-n2

3. sdl2.sh - fixes for rpi3+4 and vero4k

4. mess-dev.sh and mame-dev.sh - added missing dependencies and flags

5. love-.0.10.2.sh - added missing patch

6. barrage.sh - fixed typo

7. open-omf2097.sh - rewritten to build from source

8. openrct2.sh - new untested OpenRollercoasterTycoon2


ARES-Setup Changelog  Mar 1 2020

In sync with RetroPie 4.5.14

Function Changes

1. runcommand - fix running software via runcommand that use X outside of Emulation Station

 * if no TTY var is set (as with the ES launch script), try and get it - otherwise xinit will try and start on a VT we don't have access to
 * use a single TTY var name in emulationstation launch script -Jools Wills

2. amiberry - target dispmanx + sdl2 for rpi4 due to kmsdrm driver issues with amiberry 3.1.2 -Jools Wills

3. setup - show platform in main RetroPie-Setup menu header 

4. runcommand: override locale settings for printf

5. packages - expand platform flags to allow excluding all platforms andincluding certain ones

This should be backward compatible with the previous behaviour. By default all platforms are included.
Previously if you wanted a rpi+videocore only module you had to use lots of flags to exclude other platforms - eg

!x86 !mali !kms

which isn't ideal as new platforms are added.
Now this can be changed now by using a !all flag 

!all videocore

which would first exclude all platforms, then enable the module just for videocore.  -Jools Wills

6. image - add parameter to generate berryboot images (disabled by default)



ARES-Setup Changelog  Feb 27 2020

In sync with RetroPie 4.5.14

Functions Removed

General: 

1. Fix mali removed from all scripts. (This should get fixed in the OS itself) Rp64 and N2
2. Automatically Copying RetroFE system art folders into the roms folders has been removed. (Need to find a better way)
3. All Systems reverted to "stock" configurations
4. No PSP Cheats
5. Installing cores, emulators, ports from binary
6. Retroarch overlays and cheats script functions are missing. 

Runcommand:

1. BIOS Check
2. Launching videos
3. OGST Case



