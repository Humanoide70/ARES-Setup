#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="fancontrol"
rp_module_desc="Change the fan settings to control cooling and fan noise."
rp_module_section="config"
rp_module_flags="!all odroid-xu"

function gui_fancontrol() {
    while true; do
        local cmd=(dialog --backtitle "$__backtitle" --menu "Fan Control" 22 86 16)
        local options=(
            1 "Fan Control 1 - Default"
            2 "Fan Control 2 - Low"
            3 "Fan Control 3 - Medium"
			4 "Fan Control 4 - Full"
        )
        local choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
        [[ -z "$choice" ]] && break
        if [[ -n "$choice" ]]; then
            case "$choice" in
                1) 
     				echo step_wise | sudo tee /sys/devices/virtual/thermal/thermal_zone*/policy
					echo 0 | sudo tee /sys/devices/platform/pwm-fan/hwmon/hwmon0/pwm1
                    sudo cp -r -f $scriptdir/scriptmodules/supplementary/fancontrol/fan1-default/rc.local /etc
                    printMsgs "dialog" "Fan is now restored to the factory Odroid settings."
                    ;;
                2)
				    echo user_space | sudo tee /sys/devices/virtual/thermal/thermal_zone*/policy
                    echo 120 | sudo tee /sys/devices/platform/pwm-fan/hwmon/hwmon0/pwm1
                    sudo cp -r -f $scriptdir/scriptmodules/supplementary/fancontrol/fan2-low/rc.local /etc
                    printMsgs "dialog" "Fan is now set to a low constant speed compared to the factory Odroid settings.\n\nNOTE: PERFORM AT YOUR OWN RISK. NO IMPLIED WARRANTIES."
                    ;;
                3)
				    echo user_space | sudo tee /sys/devices/virtual/thermal/thermal_zone*/policy
                    echo 180 | sudo tee /sys/devices/platform/pwm-fan/hwmon/hwmon0/pwm1
                    sudo cp -r -f $scriptdir/scriptmodules/supplementary/fancontrol/fan3-medium/rc.local /etc
                    printMsgs "dialog" "Fan is now set to a medium constant speed compared to the factory Odroid settings. \n\nNOTE: PERFORM AT YOUR OWN RISK. NO IMPLIED WARRANTIES."
                    ;;
				4)
				    echo user_space | sudo tee /sys/devices/virtual/thermal/thermal_zone*/policy
                    echo 255 | sudo tee /sys/devices/platform/pwm-fan/hwmon/hwmon0/pwm1
                    sudo cp -r -f $scriptdir/scriptmodules/supplementary/fancontrol/fan4-full/rc.local /etc
                    printMsgs "dialog" "Fan is now set to the most AGGRESSIVE COOLING RATE compared to the factory Odroid settings. The fan will become noticeably loud.\n\nNOTE: PERFORM AT YOUR OWN RISK. NO IMPLIED WARRANTIES."
                    ;;
            esac
        fi
    done
}